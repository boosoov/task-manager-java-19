package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    void add(@NotNull String userId, @NotNull Project project);

    Project remove(@NotNull String userId, @NotNull Project project);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    @Nullable
    Project findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project removeOneByName(@NotNull String userId, @NotNull String name);

    void load(@NotNull Collection<Project> projects);

    void load(@NotNull Project... projects);

    @NotNull
    Project merge(@NotNull Project project);

    void merge(@NotNull Collection<Project> projects);

    void merge(@NotNull Project...projects);

    void clearAll();

    @NotNull
    List<Project> getListProjects();

}
