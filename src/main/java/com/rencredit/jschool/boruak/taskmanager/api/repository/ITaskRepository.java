package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    @NotNull
    List<Task> findAll(@NotNull String userId);

    void add(@NotNull String userId, @NotNull Task task);

    @Nullable
    Task remove(@NotNull String userId, @NotNull Task task);

    void clear(@NotNull String userId);

    @Nullable
    Task findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task removeOneByName(@NotNull String userId, @NotNull String name);

    void load(@NotNull Collection<Task> tasks);

    void load(@NotNull Task... tasks);

    @NotNull
    Task merge(@NotNull Task task);

    void merge(@NotNull Collection<Task> tasks);

    void merge(@NotNull Task...tasks);

    void clearAll();

    @NotNull
    List<Task> getListTasks();

}
