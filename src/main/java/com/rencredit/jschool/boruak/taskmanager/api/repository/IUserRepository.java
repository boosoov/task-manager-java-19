package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface IUserRepository {

    @NotNull
    List<User> findAll();

    @NotNull
    User add(@NotNull User user);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String name);

    @Nullable
    User removeById(@NotNull String id);

    @Nullable
    User removeByLogin(@NotNull String name);

    @Nullable
    User removeByUser(@NotNull User user);

    void load(@NotNull Collection<User> users);

    void load(@NotNull User... users);

    @NotNull
    User merge(@NotNull User user);

    void merge(@NotNull Collection<User> users);

    void merge(@NotNull User...users);

    void clearAll();

    @NotNull
    List<User> getListUsers();

}
