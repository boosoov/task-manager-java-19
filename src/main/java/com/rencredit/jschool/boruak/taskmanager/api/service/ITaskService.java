package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void create(@Nullable String userId, @Nullable Task task);

    void remove(@Nullable String userId, @Nullable Task task);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    void clear(@Nullable String userId);

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Task findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Task removeOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    void load(@Nullable Collection<Task> tasks);

    void load(@Nullable Task... tasks);

    @NotNull
    Task merge(@Nullable Task task);

    void merge(@Nullable Collection<Task> tasks);

    void merge(@Nullable Task...tasks);

    void clearAll();

    @NotNull
    List<Task> getListTasks();

}
