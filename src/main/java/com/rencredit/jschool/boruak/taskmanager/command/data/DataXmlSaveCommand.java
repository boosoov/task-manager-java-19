package com.rencredit.jschool.boruak.taskmanager.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.rencredit.jschool.boruak.taskmanager.api.service.IDomainService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.constant.DataConstant;
import com.rencredit.jschool.boruak.taskmanager.dto.Domain;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

public class DataXmlSaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA XML SAVE]");
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);

        @NotNull final File file =  new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String xml = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConstant.FILE_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
