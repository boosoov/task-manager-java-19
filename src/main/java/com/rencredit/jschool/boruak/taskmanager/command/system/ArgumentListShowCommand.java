package com.rencredit.jschool.boruak.taskmanager.command.system;

import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class ArgumentListShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
        @NotNull final String[] arguments = commandService.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

}
