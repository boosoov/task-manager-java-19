package com.rencredit.jschool.boruak.taskmanager.command.system;

import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyCommandException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal command.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
        @NotNull final Map<String, AbstractCommand> commands = commandService.getTerminalCommands();
        for(Map.Entry<String, AbstractCommand> command : commands.entrySet()){
            @NotNull final StringBuilder resultString = new StringBuilder();
            @NotNull final AbstractCommand commandValue = command.getValue();
            if(commandValue == null) throw new EmptyCommandException();
            if (!commandValue.name().isEmpty()) resultString.append(commandValue.name());
            @Nullable final String arg = commandValue.arg();
            if (arg != null && !arg.isEmpty()) resultString.append(", ").append(commandValue.arg());
            if (!commandValue.description().isEmpty()) resultString.append(": ").append(commandValue.description());
            System.out.println(resultString.toString());
        }
        System.out.println("[OK]");
    }

}
