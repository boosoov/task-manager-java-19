package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.logic.InternalProgramException;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserUpdatePasswordCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "update-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Update password.";
    }

    @Override
    public void execute() {
        System.out.println("Enter new password");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.updatePasswordById(userId, password);
        System.out.println(user);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN, Role.USER};
    }

}
