package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptyUserIdException extends RuntimeException {

    public EmptyUserIdException() {
        super("Error! User id is empty...");
    }

}
