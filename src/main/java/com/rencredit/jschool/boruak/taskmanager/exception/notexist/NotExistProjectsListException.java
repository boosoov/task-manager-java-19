package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

public class NotExistProjectsListException extends RuntimeException {

    public NotExistProjectsListException() {
        super("Error! Project list is not exist...");
    }

}
