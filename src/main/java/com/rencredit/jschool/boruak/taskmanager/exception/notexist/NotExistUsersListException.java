package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

public class NotExistUsersListException extends RuntimeException {

    public NotExistUsersListException() {
        super("Error! Users list is not exist...");
    }

}
