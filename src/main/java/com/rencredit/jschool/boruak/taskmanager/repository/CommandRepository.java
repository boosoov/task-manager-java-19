package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ICommandRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.*;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.command.data.*;
import com.rencredit.jschool.boruak.taskmanager.command.user.*;
import com.rencredit.jschool.boruak.taskmanager.command.user.auth.UserLogOutCommand;
import com.rencredit.jschool.boruak.taskmanager.command.user.auth.UserLoginCommand;
import com.rencredit.jschool.boruak.taskmanager.command.project.*;
import com.rencredit.jschool.boruak.taskmanager.command.system.*;
import com.rencredit.jschool.boruak.taskmanager.command.task.*;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void putCommand(@NotNull final String name, @NotNull final AbstractCommand command) {
        commands.put(name, command);
    }

    @NotNull
    @Override
    public String[] getCommands() {
        @NotNull final String[] result = new String[commands.size()];
        int index = 0;
        for (@Nullable Map.Entry<String, AbstractCommand> command : commands.entrySet()) {
            @NotNull final StringBuilder resultString = new StringBuilder();
            if( command == null) continue;
            if (!command.getValue().name().isEmpty()) resultString.append(command.getValue().name());
            if (!command.getValue().description().isEmpty()) resultString.append(": ").append(command.getValue().description());
            result[index] = resultString.toString();
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    @NotNull
    @Override
    public String[] getArgs() {
        @NotNull final String[] result = new String[commands.size()];
        int index = 0;
        for (Map.Entry<String, AbstractCommand> command : commands.entrySet()) {
            final StringBuilder resultString = new StringBuilder();
            @Nullable final String arg = command.getValue().arg();
            if (arg != null) {
                if (command.getValue().arg() != null && !arg.isEmpty())
                    resultString.append(command.getValue().arg());
            }
            if (!command.getValue().description().isEmpty())
                resultString.append(": ").append(command.getValue().description());
            result[index] = resultString.toString();
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    @NotNull
    @Override
    public Map<String, AbstractCommand> getTerminalCommands() {
        return commands;
    }

}
