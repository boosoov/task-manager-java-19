package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IUserRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @NotNull
    @Override
    public User add(@NotNull final User user) {
        users.add(user);
        return user;
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        for (@NotNull final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@NotNull final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User removeById(@NotNull final String id) {
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        return removeByUser(user);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        return removeByUser(user);
    }

    @Nullable
    @Override
    public User removeByUser(@NotNull final User user) {
        return (users.remove(user) ? user : null);
    }

    @Override
    public void load(@NotNull Collection<User> users) {
        clearAll();
        merge(users);
    }

    @Override
    public void load(@NotNull User... users) {
        clearAll();
        merge(users);
    }

    @NotNull
    @Override
    public User merge(@NotNull User user) {
        users.add(user);
        return user;
    }

    @Override
    public void merge(@NotNull Collection<User> users) {
        for (@NotNull final User user : users) merge(user);
    }

    @Override
    public void merge(@NotNull User... users) {
        for (@NotNull final User user : users) merge(user);
    }

    @Override
    public void clearAll() {
        users.clear();
    }

    @NotNull
    @Override
    public List<User> getListUsers() {
        return users;
    }

}
