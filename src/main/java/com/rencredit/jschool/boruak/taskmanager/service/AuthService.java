package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IAuthRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyEmailException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyRoleException;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IAuthRepository authRepository;

    public AuthService(@NotNull final IUserService userService, @NotNull final IAuthRepository authRepository) {
        this.userService = userService;
        this.authRepository = authRepository;
    }

    @Nullable
    @Override
    public String getUserId() {
        return authRepository.getUserId();
    }

    @Override
    public void checkRoles(@Nullable Role[] roles) {
        if (roles ==null || roles.length == 0) return;
        @Nullable final String userId = getUserId();
        @Nullable final User user = userService.getById(userId);
        if (user == null) throw new DeniedAccessException();
        @Nullable final Role role = user.getRole();
        for (final Role item : roles) if (role.equals(item)) return;
        throw new DeniedAccessException();
    }

    @Override
    public void logIn(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @Nullable final User user = userService.getByLogin(login);
        if (user == null) throw new UnknownUserException();
        if (user.isLocked()) throw new DeniedAccessException();
        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new IncorrectHashPasswordException();
        if (!passwordHash.equals(user.getPasswordHash())) throw new IncorrectPasswordException();
        authRepository.logIn(user);
    }

    @NotNull
    @Override
    public User registration(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        return userService.add(login, password);
    }

    @NotNull
    @Override
    public User registration(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();

        return userService.add(login, password, email);
    }

    @NotNull
    @Override
    public User registration(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        return userService.add(login, password, role);
    }

    @Override
    public void logOut() {
        authRepository.logOut();
    }

}
